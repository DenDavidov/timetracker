var express = require('express');
var router = express.Router();

var data = {
  categories: [
    {id: '1', name: 'Отдых', description: 'Все, что связано с отдыхом'},
    {id: '2', name: 'Работа', description: 'Все, что связано с работой'},
    {id: '3', name: 'Учеба', description: 'Все, что связано с учебой'}
  ],
  worklog: [
    {id: '1', categoryId: '1', description: 'Сон', startDate: 0, finishDate: 56040},
    {id: '2', categoryId: '2', description: 'Программирование', startDate: 56040, finishDate: 67040},
    {id: '3', categoryId: '1', description: 'Перекур', startDate: 67040, finishDate: 68050}
  ],
  currentActivity: {
    id: '1',
    startDate: 67040,
    status: 'paused'
  }
};




/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/data', function(req, res, next) {
  res.send(data);
});

module.exports = router;
