/**
 * Created by DavDA on 17.07.2015.
 */
"use strict";

var timers = {animationTimer:{value:0,func:null}};

var dataService;

//массив объектов данных, заменится на response с сервера
var DataService = function() {
    this.data = {};

    this.a1 = 0;

};

DataService.prototype.refresh = function() {
    var b1 = this.a1
    console.log(this.a1);
    $.ajax({
        url: '/data',
        success: function (res) {
            dataService.data = res;
            init();
            b1 = 1;
            console.log(b1);
            setData(b1);
        }
    });

    var setData = function(b1) {
        console.log(this);
    }
    
};

DataService.prototype.getA1 = function() {
    console.log(this.a1);
}


var USE_TIMER_SIMPLE_MODE = true;//нулевые старшие разряды не заполняются нулями
var INSERT_ZERO = true;//нулевые старшие разряды не заполняются нулями

$(document).ready(function() {
    dataService = new DataService();
    dataService.refresh();
    $("#tabs").tabs();



    $(".category-list").sortable({
        stop: sortCategories
    });
});

function init() {
    initButtons();
    initWorkLog();
    initCategoriesList();
}

function initButtons() {
    $("#ButtonWrapper").html('');
    console.log(dataService.data);
    dataService.data.categories.forEach(function(category){
        var button =  $("<input type='button' class='category'/>").appendTo($("#ButtonWrapper"));
        button.attr('value',category.name);
        button.attr('tt-category',category.id);
        button.on('click', function() {
            startActivity($(this).attr('tt-category'));
        });
    });
}

function initWorkLog() {
    var workLogTable = $('.worklog').find('tbody');
    dataService.data.worklog.forEach(function(activity) {
        var tr = $('<tr></tr>').appendTo(workLogTable);
        tr.append($('<td></td>').
            html(activity.description));
        tr.append($('<td></td>').
            html(getCategoryNameById(activity.categoryId)));
        tr.append($('<td></td>').addClass('right').
            html(translateSecondsToTime(activity.startDate)));
        tr.append($('<td></td>').addClass('right').
            html(translateSecondsToTime(activity.finishDate)));
        tr.append($('<td></td>').addClass('right').
            html(translateSecondsToTime(activity.finishDate - activity.startDate)));
    });
    $(".worklog").tablesorter();
}

function initCategoriesList() {
    dataService.data.categories.forEach(function(category){
        var li =  $("<li></li>").attr('idr',category.id).html(category.name + ' (' + category.description + ')').appendTo($(".category-list"));
    });
}

//обработчики событий
function startActivity(category) {
    var statusText = getCategoryNameById(category);
    var description = $("[name='description']").val();
    if (description) statusText += " (" + description + ")";
    $('.statusbar-description').
        html(statusText);
    $("[name='description']").val('').focus();

    timers.animationTimer.value = 0;
    $('.statusbar-timer').
        html(timers.animationTimer.value, USE_TIMER_SIMPLE_MODE);
    clearInterval(timers.animationTimer.func);
    timers.animationTimer.func = setInterval(function() {
        timers.animationTimer.value++;
        $('.statusbar-timer').
            html(translateSecondsToTime(timers.animationTimer.value, true));
    }, 1000)
    dataService.getA1();
}

//работа с категориями
function sortCategories() {
    var newSort = [];
    $(".category-list li").each(function(){
        newSort.push($(this).attr('idr'));
    });
    dataService.data.categories = resortArray(dataService.data.categories, newSort);
    initButtons();
}

function getCategoryNameById(categoryId) {
    return findInArray(dataService.data.categories,'id',categoryId, 'name');
}

/*
Пересортировывает массив в соответствии с новым порядком идентификаторов
 */
function resortArray(arr, newSort){
    var result = [];
    newSort.forEach(function(el){
        result.push(findInArray(arr, 'id', el));
    });
    return result;
}

//Служебные функции для отображения времени
function translateSecondsToTime(seconds, simpleMode){
    //simpleMode - отображать без нулевых значений часов и минут
    var result = '',
        delimiter = ':',
        h = Math.floor(seconds / 3600),
        m = Math.floor((seconds % 3600)/60),
        s = Math.floor((seconds % 3600) % 60);
    if (simpleMode) {
        result = h ? h + delimiter : '';
        result += (m || h) ? insertZero(m, h) + delimiter : '';
        result += insertZero(s, m || h);
    } else {
        result = insertZero(h, INSERT_ZERO) + delimiter
            + insertZero(m, INSERT_ZERO) + delimiter
            + insertZero(s, INSERT_ZERO);
    }
    return result;
}

function insertZero(val, prevVal) {
    return (val<10&&prevVal)? '0'+val : val;
}

//Утилитарные функции для работы с массивами объектов

function findInArray(arr, searchParam, searchValue, returnParam) {
    var result = null;
    arr.forEach(function(el) {
        if (el[searchParam] === searchValue) {
            result = (returnParam)?el[returnParam]:el;
        }
    });
    return result;
}